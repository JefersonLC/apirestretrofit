package com.iscodem.apirestretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.iscodem.apirestretrofit.interfaces.IJsonPlaceHolderApi;
import com.iscodem.apirestretrofit.model.Pokemon;
import com.iscodem.apirestretrofit.model.PokemonResult;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    TextView txtContenidoJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, ">>>Metodo onCreate()");

        txtContenidoJson = findViewById(R.id.txtContenidoJson);

        this.getPokemon(); //LLamada al retrofit
    }

    private void getPokemon(){
        Log.d(TAG,">>>Metodo MainActivity.getPokemon()");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IJsonPlaceHolderApi iJsonPlaceHolderApi = retrofit.create(IJsonPlaceHolderApi.class);

        Call<PokemonResult> call = iJsonPlaceHolderApi.getPokemon();

        call.enqueue(new Callback<PokemonResult> (){
            @Override
            public void onResponse(Call<PokemonResult> call, Response<PokemonResult> response) {
                Log.d(TAG,">>>Metodo call.enqueue.onResponse()");
                if(!response.isSuccessful()){
                    txtContenidoJson.setText("Codigo ok: "+response.code());
                    return;
                }
                PokemonResult res = response.body();
                ArrayList<Pokemon> lsPokemon = res.getResults();

                String contenido = "";

                for(int i=0; i<lsPokemon.size();i++){
                    Pokemon p = lsPokemon.get(i);
                    if(!contenido.contains((i+1)+"") || !contenido.contains(p.getName())){
                        contenido+="Numero: "+ (i + 1) +"\n";
                        contenido+="Nombre: "+ p.getName() +"\n\n";
                    }
                    txtContenidoJson.append(contenido);
                }
            }

            @Override
            public void onFailure(Call<PokemonResult> call, Throwable t) {
                Log.d(TAG,">>>Metodo call.enqueue.onFailure()");
                txtContenidoJson.setText("ERROR ok: "+t.getMessage());
            }
        });
    }
}
