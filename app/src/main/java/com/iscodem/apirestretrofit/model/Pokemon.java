package com.iscodem.apirestretrofit.model;

public class Pokemon {
    private String name;

    //Constructores
    public Pokemon() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
