package com.iscodem.apirestretrofit.interfaces;

import com.iscodem.apirestretrofit.model.PokemonResult;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IJsonPlaceHolderApi {
    //Metodo encargado de obtener la informacion del API REST
    @GET("pokemon?offset=0&limit=10")
    Call<PokemonResult> getPokemon();
}
